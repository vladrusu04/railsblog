# README #

This Application is a rails tutorial Blog

* Rails tutorial Blog
* 1.0.0

### How do I get set up? ###

* Install [Ruby](http://rubyinstaller.org/)

* Dependencies
    * rails
    * ruby devkit
    * gem

* Database configuration
    * rake db:migrate